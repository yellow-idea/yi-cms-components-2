module.exports = {
    "transpileDependencies": [
        "vuetify"
    ],
    publicPath: process.env.VUE_APP_CDN_URL,
    productionSourceMap: false, 
}